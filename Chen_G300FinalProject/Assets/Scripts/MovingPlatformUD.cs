﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformUD : MonoBehaviour {

    public float speed;
    bool moveUP = true;
    float temp;
    public float distance;

    private void Start()
    {
        Vector3 pos = transform.position;
        temp = pos.y;
    }

    void Update()
    {
        Vector3 currentPos = transform.position;

        if (currentPos.y > temp + distance)
            moveUP = false;
        if (currentPos.y < temp - distance)
            moveUP = true;
        if (moveUP)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + speed * Time.deltaTime);
        }
        else
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - speed * Time.deltaTime);
        }
    }
}

