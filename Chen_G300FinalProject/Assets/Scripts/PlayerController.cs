﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float maxSpeed;
    bool grounded = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

    Rigidbody2D rb;
    Animator myAnim;
    bool facingRight;

    public float dashspeed;
    private float dashTime;
    public float startdashtime;
    private int direction;
    private bool dashing;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        facingRight = true;
        dashing = false;
        dashTime = startdashtime;
    }

    private void Update()
    {
        if (grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            myAnim.SetBool("IsGrounded", grounded);
            rb.AddForce(new Vector2(0, jumpHeight));
        }
        if (direction == 0)
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.LeftShift))
            {
                direction = 1;
                dashing = true;
            }
            else if (Input.GetKey(KeyCode.D) && Input.GetKeyDown(KeyCode.LeftShift))
            {
                direction = 2;
                dashing = true;
            }
        }
        else
        {
            if (dashTime <= 0)
            {
                float move = Input.GetAxis("Horizontal");
                rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);
                direction = 0;
                dashTime = startdashtime;
                //rb.velocity = Vector2.zero;
            }
            else
            {
                dashTime -= Time.fixedDeltaTime;

                if (direction == 1)
                {
                    rb.velocity = Vector2.left * dashspeed;
                }else if (direction == 2)
                {
                    rb.velocity = Vector2.right * dashspeed;
                }
            }
            dashing = false;
        }
        
    }

    void flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
     
        if (dashing == false)
        {
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
            myAnim.SetBool("IsGrounded", grounded);
            myAnim.SetFloat("VerticalSpeed", rb.velocity.y);
            float move = Input.GetAxis("Horizontal");
            myAnim.SetFloat("Speed", Mathf.Abs(move));
            if (direction == 0)
                rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);

            if (move > 0 && !facingRight)
            {
                flip();
            }
            else if (move < 0 && facingRight)
            {
                flip();
            }
        }
        
        
    }
}
