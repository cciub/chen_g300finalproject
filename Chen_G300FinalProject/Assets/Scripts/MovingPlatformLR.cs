﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformLR : MonoBehaviour {

    float dirX, speed = 3f;
    bool moveRight = true;
    float temp;
    public float distance;
    public Transform player;

    private void Start()
    {
        Vector3 pos = transform.position;
        temp = pos.x;
    }

    // Update is called once per frame
    void Update () {
        Vector3 currentPos = transform.position;
        if (currentPos.x > temp + distance)
            moveRight = false;
        if (currentPos.x < temp - distance)
            moveRight = true;
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + speed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x - speed * Time.deltaTime, transform.position.y);
        }

    }


    void OnTriggerEnter(Collider other)
    {
        //other.transform.parent = gameObject.transform;
     if (other.gameObject.tag == "Player")
        {
            player.transform.parent = this.transform;
        }
           
    }

    void OnTriggerExit(Collider other)
    {
        //other.transform.parent = null;
           if (other.gameObject.tag == "Player")
           {
               player.transform.parent = null;
           }
    }
}
