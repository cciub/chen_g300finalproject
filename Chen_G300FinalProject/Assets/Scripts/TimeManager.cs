﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour
{
    public static float time;
    Animator anim;
    //AudioSource playerAudio;
    PlayerController playerMovement;
    //bool IsDead;

    public Text currentTime;
    

    void Awake()
    {
        currentTime = GetComponent<Text>();
        time = 120;
    }


    void Update()
    {
        if (time > 0)
        {
            if (time <= 10) { currentTime.color = Color.red; }
            time -= Time.deltaTime;

            currentTime.text = "Time : " + time.ToString("F");
            // if (time >=120) { stop = true; }   //1-minutes <= 0 && 60-seconds <= 0 && 1000-fraction <= 0)
        }
        else
        {
        //    IsDead = true;
            Time.timeScale = 0f;
        }
    }



}
