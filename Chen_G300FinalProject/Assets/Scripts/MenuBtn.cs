﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBtn : MonoBehaviour {

    public static bool isPaused = false;
    public GameObject PauseMenuUI;
    public GameObject GameoverMenuUI;
    public TimeManager currentTime;

    // Update is called once per frame
    void Awake()
    {
        PauseMenuUI.SetActive(false);
        GameoverMenuUI.SetActive(false);
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused) { Resume(); }
            else { Pause(); }
        }
        string x = currentTime.currentTime.text;
        if (x == "Time : 0.00")
        {
            GameoverMenuUI.SetActive(true);
            Time.timeScale = 0f;
        }

    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }


    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void Exit()
    {
        Application.Quit();
    }
}

