﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour {
    public GameObject player;
    public float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float distX = this.transform.position.x - player.transform.position.x;
        float distY = this.transform.position.y - player.transform.position.y; 
        Debug.Log("Coordinate (" + distX + ", " + distY + ")");
        if ( distX <= 11f && distY <= 0f)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, 0), player.transform.position, speed * Time.deltaTime);
        }
    }
}
